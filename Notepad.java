import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime; 

public class Notepad extends Frame implements ActionListener{
	TextArea textcontent;
	boolean saved = false;
	String file ="";
	String directory = "";
	String copied = "";
	int fromIndex = -1;
	Notepad(){
		super("Untitled");
		setSize(700,700);
		setVisible(true);
		textcontent = new TextArea();
		setLayout(new BorderLayout());
		add(textcontent,BorderLayout.CENTER);

		MenuBar mbar = new MenuBar();
		setMenuBar(mbar);

		Menu filemenu = new Menu("File");
		Menu editmenu = new Menu("Edit");
		Menu view = new Menu("View");

		mbar.add(filemenu);
		mbar.add(editmenu);

		MenuItem new1 = new MenuItem("New");
		MenuItem newwindow = new MenuItem("New Window");
		MenuItem open = new MenuItem("Open");
		MenuItem save = new MenuItem("Save");
		MenuItem saveas = new MenuItem("Save as");
		MenuItem dash = new MenuItem("-");
		MenuItem exit = new MenuItem("Exit");

		filemenu.add(new1);
		filemenu.add(newwindow);
		filemenu.add(open);
		filemenu.add(save);
		filemenu.add(saveas);
		filemenu.add(dash);
		filemenu.add(exit);

		MenuItem cut = new MenuItem("Cut");
		MenuItem copy = new MenuItem("Copy");
		MenuItem paste = new MenuItem("Paste");
		MenuItem replace = new MenuItem("Replace");
		MenuItem find = new MenuItem("Find");
		MenuItem dateAndTime = new MenuItem("Time/Date");
		CheckboxMenuItem wordwrap = new CheckboxMenuItem("Word Wrap",true);

		editmenu.add(cut);
		editmenu.add(copy);
		editmenu.add(paste);
		editmenu.addSeparator();
		editmenu.add(find);
		editmenu.add(replace);
		editmenu.add(dateAndTime);

		view.add(wordwrap);

		new1.addActionListener(this);
		newwindow.addActionListener(this);
		open.addActionListener(this);
		save.addActionListener(this);
		exit.addActionListener(this);
		cut.addActionListener(this);

		copy.addActionListener(this);
		paste.addActionListener(this);
		find.addActionListener(this);
		replace.addActionListener(this);
		dateAndTime.addActionListener(this);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				dispose();
			}
		});


	}
	public void openFileDialog(){
		FileDialog fd = new FileDialog(this,"Open File",FileDialog.LOAD);
		fd.setVisible(true);
		setTitle(fd.getFile());
		textcontent.setText("");
		File f = new File(fd.getDirectory()+fd.getFile());
		String text;
		try{
			FileInputStream fis = new FileInputStream(f);
			BufferedReader br = new BufferedReader(new InputStreamReader(fis));
			while((text = br.readLine())!= null){
				textcontent.append(text += "\n");
			}

		}catch(FileNotFoundException e){}
		catch(IOException e){}
	}
	public void saveFileDialog(){
		
		String content = textcontent.getText();
		byte b[] = content.getBytes();
		if(!saved){
			FileDialog fd = new FileDialog(this,"Save File",FileDialog.SAVE);
			fd.setVisible(true);
			setTitle(fd.getFile());
			if(fd.getDirectory()!=null && fd.getFile()!=null){
				File f = new File(fd.getDirectory()+fd.getFile());

				try{
					FileOutputStream fout = new FileOutputStream(f);
					fout.write(b);
					fout.close();
				}catch(FileNotFoundException e){
					System.out.println("File not found!");
				}catch(IOException e){
					System.out.println("Some Error occured!");
				}	
			}
			file = fd.getFile();
			directory = fd.getDirectory();
			saved = true;	
		}else{
				content = textcontent.getText();
				b = content.getBytes();
				System.out.println(file);
				System.out.println(directory);
			
				
				File f = new File(""+directory + file);

				try{
					FileOutputStream fout = new FileOutputStream(f);
					fout.write(b);
					fout.close();
				}catch(FileNotFoundException e){
					System.out.println("File not found!");
				}catch(IOException e){
					System.out.println("Some Error occured!");
				}
			
		}

	}
	public void newFile(){
		if(!textcontent.getText().equals(""))
			new SaveDialogbox(this,"Title",this);
		
	}
	public void copyText(){
		copied = textcontent.getSelectedText();
		System.out.print(copied);
	}
	public void pasteText(){
		textcontent.insert(copied+" ",textcontent.getCaretPosition());
		System.out.println("Pasted");
	}
	public void cutText(){
		String selected = textcontent.getSelectedText();
		int startindex = textcontent.getText().indexOf(selected);
		textcontent.replaceRange("",startindex,startindex+selected.length());
		copied = selected;
	}
	public void replaceDialogbox(){
		new replaceDialogBox(this,"Find",this);
	}
	
	public void actionPerformed(ActionEvent ae){
		if(ae.getActionCommand().equals("New")){
			newFile();
		}else if(ae.getActionCommand().equals("New Window")){
			new Notepad();
		}else if(ae.getActionCommand().equals("Open")){
			openFileDialog();
		}else if(ae.getActionCommand().equals("Save")){
			saveFileDialog();
		}else if(ae.getActionCommand().equals("Replace")){
			replaceDialogbox();
		}else if(ae.getActionCommand().equals("Copy")){
			copyText();
		}else if(ae.getActionCommand().equals("Paste")){
			pasteText();
		}else if(ae.getActionCommand().equals("Cut")){
			cutText();
		}else if(ae.getActionCommand().equals("Find")){
			new findDialogBox(this,"Find",this);
		}else if(ae.getActionCommand().equals("Time/Date")){
			new DateAndTime(this);
		}
	}

	public static void main(String[] args) {
		new Notepad();
	}
}

class SaveDialogbox extends Dialog implements ActionListener{

	Notepad refObj;
	Button yes, no, cancel;
	Label savelabel;
	SaveDialogbox(Frame parent, String title, Notepad ref){
		
		super(parent,title,true);
		refObj = ref;
		setLayout(new FlowLayout());
		savelabel = new Label("Do you want to save your changes in the file?");
		yes = new Button("Save Changes");
		no = new Button("Don't Save");
		cancel = new Button("Cancel");

		add(savelabel);
		add(yes);
		add(no);
		add(cancel);

		yes.addActionListener(this);
		no.addActionListener(this);
		cancel.addActionListener(this);

		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				dispose();
			}
		});
		setSize(300,150);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent ae){
		if(ae.getSource() == yes){
			refObj.saveFileDialog();
			refObj.dispose();
			new Notepad();
			dispose();
		}else if(ae.getSource() == no){
			// refObj.textcontent.setText("");
			refObj.dispose();
			new Notepad();
			dispose();
		}else if(ae.getSource() == cancel){
			dispose();
		}	
	}
}

class replaceDialogBox extends Dialog implements ActionListener{
	
	Label find,replace;
	TextField findtext, replacetext;
	Button ok,cancel,replaceAll;
	Notepad refObj;
	replaceDialogBox(Frame parent, String title,Notepad ref){
		super(parent,title);
		refObj = ref;
		setLayout(new FlowLayout());
		find = new Label("Find");
		replace = new Label("Replace");
		findtext = new TextField(8);
		replacetext = new TextField(8);
		ok = new Button("Replace");
		cancel = new Button("Cancel");
		replaceAll = new Button("Replace All");

		add(find);
		add(findtext);
		add(replace);
		add(replacetext);
		add(ok);
		add(replaceAll);
		add(cancel);

		ok.addActionListener(this);
		replaceAll.addActionListener(this);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				dispose();
			}
		});

		setSize(500,500);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent ae){
		String findT = findtext.getText();
		String replaceT = replacetext.getText();
		String alltext = refObj.textcontent.getText();
		refObj.textcontent.requestFocus();
		if(ae.getSource() == ok){
			alltext = alltext.replaceFirst(findT,replaceT);
			refObj.textcontent.setText(alltext);

		}else if(ae.getSource() == replaceAll){
			alltext = alltext.replaceAll(findT,replaceT);
			refObj.textcontent.setText(alltext);
		}

	}
}

class findDialogBox extends Dialog implements ActionListener{

	Notepad refObj;
	TextField findtext;
	Button find,findNext,findPrev;
	String findt;
	int endindex, startindex;
	int fromIndex = -1;
	findDialogBox(Frame parent, String title, Notepad ref){
		super(parent,title);
		refObj = ref;
		findtext = new TextField(15);
		find =  new Button("Find");
		findNext = new Button("Find Next");
		findPrev = new Button("Find Previous");
		
		endindex = 0;
		setLayout(new FlowLayout());
		add(findtext);
		add(find);
		add(findNext);
		add(findPrev);

		find.addActionListener(this);
		findNext.addActionListener(this);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent we){
				dispose();
			}
		});

		setSize(200,200);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent ae){
		findt = findtext.getText();	
		startindex = refObj.textcontent.getText().replaceAll("\n","").indexOf(findt,endindex);

		if(ae.getSource() == find){
			System.out.println("found");
			//startindex = refObj.textcontent.getText().replaceAll("\n","").indexOf(findt,endindex);
			System.out.println(startindex);
			System.out.println(endindex);
			System.out.println(startindex+findt.length());
			if(startindex != -1){
				refObj.textcontent.requestFocus();
				refObj.textcontent.select(startindex,startindex+findt.length());
				endindex = startindex+findt.length();
			}else{
				endindex = 0;
			}
		
		}else if(ae.getSource() == findNext){
			//startindex = refObj.textcontent.getText().replaceAll("\n","").indexOf(findt,endindex);
			if(startindex != -1){
				refObj.textcontent.requestFocus();
				refObj.textcontent.select(startindex,startindex+findt.length());
				endindex = startindex+findt.length();
			}else{
				endindex = 0;
			}
		} else if (ae.getSource() == findPrev) {
            findt = findtext.getText();
            if (startindex == -1) {
                startindex = refObj.textcontent.getText().length() - 1;
            }

            startindex = refObj.textcontent.getText().replaceAll("\n", "").lastIndexOf(findt, fromIndex);
            System.out.println(startindex);
            if (startindex != -1) {
                refObj.textcontent.requestFocus();
                refObj.textcontent.select(startindex, startindex + findt.length());
                fromIndex = startindex - findt.length();
            } else {
                fromIndex = 0;
            }

        }
			
		
			
		
	}
	
}
class DateAndTime{
		Notepad refObj;
		DateAndTime(Notepad obj){
			refObj = obj;
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
   		LocalDateTime now = LocalDateTime.now();
   		refObj.textcontent.insert(dtf.format(now),refObj.textcontent.getCaretPosition());
		}
		
	}



